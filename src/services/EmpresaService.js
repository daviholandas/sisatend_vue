import { http } from 'boot/axios'

export default {
  async getEmpresas(page){
    return await http.get(`empresas?page=${page}`)
  },

  async findEmpresa(empresaNome){
    return await http.get(`empresas/${empresaNome}`)
  },

  async getValoresExamesEmpresa(codigoEmpresa){
    return await http.get(`valores/${codigoEmpresa}`)
  },

  async getRiscosEmrpesa(codigoRisco){
    return await http.get(`riscos/${codigoRisco}`)
  }
}